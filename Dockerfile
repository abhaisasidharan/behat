FROM ubuntu:latest

COPY docker-image-setup.sh /tmp/docker-image-setup.sh
RUN chmod +x /tmp/docker-image-setup.sh
RUN /tmp/docker-image-setup.sh
ADD config.yaml /root/.platformsh/config.yaml
ADD apitoken /root/.platformsh/apitoken
RUN mkdir -p /root/.ssh
ADD id_rsa /root/.ssh/id_rsa
ADD id_rsa.pub /root/.ssh/id_rsa.pub
RUN chmod 400 /root/.ssh/id_rsa
ADD ssh-config /root/.ssh/config
