echo "---Updating package list---"
apt-get update
apt-get install -y apt-utils
apt-get install -y software-properties-common

echo "---Installing php 7.1---"
add-apt-repository -y ppa:ondrej/php
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y --allow-unauthenticated php7.1 libapache2-mod-php7.1 php7.1-common php7.1-mbstring php7.1-xmlrpc php7.1-soap php7.1-gd php7.1-xml php7.1-intl php7.1-mysql php7.1-cli php7.1-mcrypt php7.1-zip php7.1-curl

echo "---Installing some base packages---"
apt-get -y --allow-unauthenticated install unzip xvfb libxi6 libgconf-2-4 vim curl build-essential git parallel

echo "---Install composer---"
if [ ! -f /usr/local/bin/composer ]; then
  curl --silent https://getcomposer.org/installer | php
  mv composer.phar /usr/local/bin/composer
fi

echo "---Installing platformsh cli---"
curl -sS https://platform.sh/cli/installer | php
. ~/.bashrc
echo $PLATFORMSH_CLI_TOKEN
echo "----  -----"

echo "---Installing Drush---"
git clone https://github.com/drush-ops/drush.git /usr/local/src/drush
cd /usr/local/src/drush
ln -s /usr/local/src/drush/drush /usr/bin/drush
composer install
cd

echo "---Installing java 8---"
add-apt-repository -y ppa:webupd8team/java
apt-get update
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections
apt-get -y install oracle-java8-installer

echo "---Installing chromedriver---"
wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
mv chromedriver /usr/bin/chromedriver
chown root:root /usr/bin/chromedriver
chmod +x /usr/bin/chromedriver
curl https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o /chrome.deb
dpkg -i /chrome.deb || apt-get install -yf
rm /chrome.deb

echo "Start selenium with chrome driver"
wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar
mv selenium-server-standalone-2.53.1.jar /usr/bin/selenium-server-standalone-2.53.1.jar
chown root:root /usr/bin/selenium-server-standalone-2.53.1.jar
chmod +x /usr/bin/selenium-server-standalone-2.53.1.jar

php -v
java -version
drush --version
platform --version
google-chrome --version

